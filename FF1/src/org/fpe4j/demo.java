package org.fpe4j;

import java.security.InvalidKeyException;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class demo {
	/**
	 *
	 * @param byteKey
	 * @param radix 基数
	 * @param maxTlen
	 * @param tweak
	 * @param plaintexts 明文   若干个整数数组
	 */
	public static void s(byte byteKey[],int radix,int maxTlen,byte tweak[],int[]...plaintexts){
		for(int[]plaintext:plaintexts)
		{
			try{
				SecretKey secretKey;//一个秘密（对称）键。 该接口的目的是为所有密钥接口分组（并提供类型安全性）。
				secretKey = new SecretKeySpec(byteKey, "AES");//从给定的字节数组构造一个秘密密钥。
				FF1 Ff1class = new FF1(radix, maxTlen);
				int[] cipher = Ff1class.encrypt(secretKey, tweak, plaintext);
				//输出
				for(int i=0; i<cipher.length; ++i){
					System.out.print(cipher[i]);
				}
				System.out.println();
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
			}
			catch (InvalidKeyException e) {
				e.printStackTrace();
			}
		}
	}


	public static void main(String[] args){
		//base = 10, {0,1,2,3,...,9}
		int radix = 10;
		int maxTlen = 20;
		int[] m1 = {0,1,2,3,4,5,6,7,8,9,1};
		int[] m2 = {0,1,2,3,4,5,6,7,8,9,1};
//	    SecretKey secretKey;
		byte[] tweak = {0,1,2,3,4,5,6,7};
		String[] strKey = {"2B", "7E", "15", "16", "28", "AE", "D2", "A6", "AB", "F7", "15", "88", "09", "CF", "4F", "3C"};
		byte[] byteKey = new byte[16];
		//密钥string转换为byte
		int strLen = strKey.length;
		for(int i = 0; i < strLen; ++i){
			byteKey[i] = (byte) Integer.parseInt(strKey[i], 16);
		}
		long startTime=System.currentTimeMillis();
		//产生secretKey
		s(byteKey,radix,maxTlen,tweak,m1,m2);
		long endTime=System.currentTimeMillis();
		System.out.println("当前程序耗时："+(endTime-startTime)+"ms");
	}
}
