package org.fpe4j;

import java.io.*;
import java.util.Arrays;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

//这是FF1
public class test1 {

	public static void s(byte byteKey[],int radix,int maxTlen,byte tweak[],int[] plaintext){
		try {
			SecretKey secretKey;
			secretKey = new SecretKeySpec(byteKey, "AES");
			FF1 Ff1class = new FF1(radix, maxTlen);
			int[] cipher = Ff1class.encrypt(secretKey, tweak, plaintext);
			FileWriter writer = null;
			try {
				// 打开一个写文件器，构造函数中的第二个参数true表示以追加形式写文件
				String fileName = "D:\\IdeaProjects\\测试代码\\FF1\\src\\org\\fpe4j\\demo1.txt";
				writer = new FileWriter(fileName, true);
				String content = Arrays.toString(cipher);
				content = content.substring(1, content.length() - 1);
				content += "\n";
				writer.write(content);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (writer != null) {
						writer.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// 输出
			for (int i = 0; i < cipher.length; ++i) {
				System.out.print(cipher[i]);
			}
			System.out.println();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		int radix = 10;
		int maxTlen = 20;
		// SecretKey secretKey;
		byte[] tweak = { 0, 1, 2, 3, 4, 5, 6, 7 };
		String[] strKey = { "2B", "7E", "15", "16", "28", "AE", "D2", "A6",
				"AB", "F7", "15", "88", "09", "CF", "4F", "3C" };
		byte[] byteKey = new byte[16];
		// 密钥string转换为byte
		int strLen = strKey.length;
		for (int i = 0; i < strLen; ++i) {
			byteKey[i] = (byte) Integer.parseInt(strKey[i], 16);
		}

		long startTime = System.currentTimeMillis();
		String myFile ="D:\\IdeaProjects\\测试代码\\FF1\\src\\org\\fpe4j\\test.txt";
		BufferedReader reader=null;
		try {
			reader = new BufferedReader(new FileReader(myFile));
			String tempString = null;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				String[] temp = tempString.split(",");
				int[] m1 = new int[temp.length];
				for(int i=0;i<temp.length;i++){
					m1[i] = Integer.parseInt(temp[i]);
				}
				s(byteKey, radix, maxTlen, tweak, m1);
			}
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("当前程序耗时：" + (endTime - startTime) + "ms");
	}

}
