/**
 * Format-Preserving Encryption
 *
 * Copyright (c) 2016 Weydstone LLC dba Sutton Abinger
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Sutton Abinger licenses this file to you under
 * the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.fpe4j;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * Common cipher functions for FF1 and FF3 based on AES.
 * 基于AES的FF1和FF3的通用密码功能。
 *
 * @author Kai Johnson
 *
 */
class Ciphers {

	/**
	 * Instance of the AES cipher in ECB mode with no padding.
	 * ECB模式下无填充的AES密码实例。
	 */
	private Cipher mAesEcbCipher;

	/**
	 * Instance of the AES cipher in CBC mode with no padding.
	 * CBC模式下无填充的AES密码实例。
	 */
	private Cipher mAesCbcCipher;

	/**
	 * Constructs a Ciphers instance with the required AES ciphers.
	 * 使用所需的AES密码构造一个Ciphers实例。
	 */
	public Ciphers() {
		try {
			//为创建 Cipher 对象，应用程序调用 Cipher 的 getInstance 方法并将所请求转换 的名称传递给它。
			mAesEcbCipher = Cipher.getInstance("AES/ECB/NoPadding");
			mAesCbcCipher = Cipher.getInstance("AES/CBC/NoPadding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			// this could happen if the JRE doesn't have the ciphers
			//如果JRE没有密码，可能会发生这种情况
			throw new RuntimeException(e);
		}
	}

	/**
	 * NIST SP 800-38G Algorithm 6: PRF(X) - Applies the pseudorandom function
	 * to the input using the supplied key.
	 * <p>
	 * Prerequisites:<br>
	 * Designated cipher function, CIPH, of an approved 128-bit block
	 * cipher;<br>
	 * Key, K, for the block cipher.
	 * <p>
	 * Input:<br>
	 * Block string, X.
	 * <p>
	 * Output:<br>
	 * Block, Y.
	 *
	 * NIST SP 800-38G算法6：PRF（X）-使用提供的键将伪随机函数应用于输入。
	 * <p>
	 *     前提条件：<br>
	 *         批准的128位块*密码的指定密码功能CIPH； <br>
	 *             块密码的密钥K。
	 *             <p>
	 *                 输入：<br>
	 *                     块字符串X。
	 *             <p>
	 *                 输出：<br>
	 *                     块Y。
	 *
	 * @param K
	 *            The AES key for the cipher function.
	 *            密码功能的AES密钥。
	 * @param X
	 *            The block string input.
	 *            块字符串输入。
	 * @return The output of the function PRF applied to the block X; PRF is
	 *         defined in terms of a given designated cipher function.
	 *         将函数PRF的输出应用于块X； PRF是根据给定的指定密码功能定义的。
	 * @throws InvalidKeyException 无效的密钥异常
	 *             If the key is not a valid AES key.
	 *             如果密钥不是有效的AES密钥。
	 */
	public byte[] prf(SecretKey K, byte[] X) throws InvalidKeyException {
		// validate K
		if (K == null)
			throw new NullPointerException("K must not be null");
		if (!K.getAlgorithm().equals("AES"))
			//public String getAlgorithm() 此方法返回算法的标准字符串名称。
			//如果K.getAlgorithm()返回值不是AES的话则证明该密钥不是具有密码功能的AES密钥
			throw new InvalidKeyException("K must contain an AES key");

		// validate X
		if (X == null)
			throw new NullPointerException("X must not be null");
		if (X.length < 1 || X.length > Constants.MAXLEN)
			throw new IllegalArgumentException(
					"The length of X is not within the permitted range of 1.." + Constants.MAXLEN + ": " + X.length);
		// 1. Let m = LEN(X)/128.
		// i.e. BYTELEN(X)/16
		int m = X.length / 16;
		// 2. Let X[1], ..., X[m] be the blocks for which X = X[1] || ... || X[m].
		// we extract the blocks inside the for loop
		// 3. Let Y(0) = bitstring(0,128), and
		byte[] Y = Common.bitstring(false, 128);//返回长度为s的位数组，其中填充了0或1s。
		// for j from 1 to m let Y(j) = CIPH(K,Y(j-1) xor X[j]).
		for (int j = 0; j < m; j++) {
			byte[] Xj = Arrays.copyOfRange(X, j * 16, j * 16 + 16);
			try {
				mAesEcbCipher.init(Cipher.ENCRYPT_MODE, K);

				Y = mAesEcbCipher.doFinal(Common.xor(Y, Xj));

			} catch (IllegalBlockSizeException | BadPaddingException e) {
				// these would be programming errors so convert to an unchecked
				// exception
				throw new RuntimeException(e);
			}
		}

		// 4. Return Y(m).
		return Y;
	}

	/**
	 * Equivalent implementation of the PRF(X) algorithm using the AES CBC cipher with a zero initialization vector.
	 * 使用具有零初始化向量的AES CBC密码等效实现PRF（X）算法。
	 * <p>
	 The PRF(X) algorithm is an implementation of CBC mode encryption with a
	 zero initialization vector. PRF(X) then extracts the last block as the
	 result. Instead of implementing CBC by hand, this method uses the Java
	 libraries to perform the same operation, and to demonstrate the
	 equivalence of the methods.
	 PRF（X）算法是具有零初始化向量的CBC模式加密的实现。然后，PRF（X）提取最后一个块作为结果。
	 该方法不是手动实现CBC，而是使用Java库执行相同的操作，并演示这些方法的等效性。
	 *
	 * @param K
	 *            The AES key for the cipher function
	 * @param X
	 *            The block string input
	 * @return The output of the function PRF applied to the block X; PRF is
	 *         defined in terms of a given designated cipher function.
	 * @throws InvalidKeyException
	 *             If the key is not a valid AES key.
	 */
	public byte[] prf2(SecretKey K, byte[] X) throws InvalidKeyException {
		// validate K
		if (K == null)
			throw new NullPointerException("K must not be null");
		if (!K.getAlgorithm().equals("AES"))
			throw new InvalidKeyException("K must be an AES key");

		// validate X
		if (X == null)
			throw new NullPointerException("X must not be null");
		if (X.length < 1 || X.length > Constants.MAXLEN)
			throw new IllegalArgumentException(
					"The length of X is not within the permitted range of 1.." + Constants.MAXLEN + ": " + X.length);

		byte[] Z;

		try {
			byte[] Y = { (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00 };

			mAesCbcCipher.init(Cipher.ENCRYPT_MODE, K, new IvParameterSpec(Y));

			Z = mAesCbcCipher.doFinal(X);

		} catch (IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			// these would be programming errors so convert to an unchecked
			// exception
			throw new RuntimeException(e);
		}

		return Arrays.copyOfRange(Z, Z.length - 16, Z.length);
	}

	/**
	 * Encrypts the input using the AES block cipher in ECB mode using the
	 * specified key.
	 * 使用指定的密钥在ECB模式下使用AES块密码对输入进行加密。
	 * <p>
	 * Although the ECB mode of operation is not explicitly mentioned in NIST SP
	 * 800-38G, it is implied by the use of the CIPH(X) function in FF1 and FF3.
	 * <p>
	 * To quote NIST SP 800-38G, "For both of the modes, the underlying block
	 * cipher shall be approved, and the block size shall be 128 bits.
	 * Currently, the AES block cipher, with key lengths of 128, 192, or 256
	 * bits, is the only block cipher that fits this profile."
	 *
	 * @param K
	 *            The AES key for the cipher function
	 * @param X
	 *            The block string input
	 * @return The output of the cipher function applied to the block X.
	 * @throws InvalidKeyException
	 *             If the key is not a valid AES key.
	 */
	public byte[] ciph(SecretKey K, byte[] X) throws InvalidKeyException {
		// validate K
		if (K == null)
			throw new NullPointerException("K must not be null");
		if (!K.getAlgorithm().equals("AES"))
			throw new InvalidKeyException("K must be an AES key");

		// validate X
		if (X == null)
			throw new NullPointerException("X must not be null");
		if (X.length < 1 || X.length > Constants.MAXLEN)
			throw new IllegalArgumentException(
					"The length of X is not within the permitted range of 1.." + Constants.MAXLEN + ": " + X.length);

		byte[] cipherText;
		try {

			mAesEcbCipher.init(Cipher.ENCRYPT_MODE, K);

			cipherText = mAesEcbCipher.doFinal(X);

		} catch (IllegalBlockSizeException | BadPaddingException e) {
			// these would be programming errors so convert to an unchecked
			// exception
			throw new RuntimeException(e);
		}

		return cipherText;
	}
}
