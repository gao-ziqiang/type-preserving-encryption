
package util;

import java.util.Random;

public class Suijishu {

	//产生不重复的随机数
	public static int[] sj(int min,int max,int n){
		int len = max-min+1;
		if(max < min || n > len){
			return null;
		}
		//初始化给定范围的待选数组
		int[] source = new int[len];
		for (int i = min; i < min+len; i++){
			source[i-min] = i;
		}
		int[] result = new int[n];
		Random rd = new Random();
		int index = 0;
		for (int i = 0; i < result.length; i++) {
			//待选数组0到(len-2)随机一个下标
			index = Math.abs(rd.nextInt() % len--);
			//将随机到的数放入结果集
			result[i] = source[index];
			//将待选数组中被随机到的数，用待选数组(len-1)下标对应的数替换
			source[index] = source[len];
		}
		return result;
	}

	//g(x)的构造
	public static int[] gx(int X[],int k){

		int[] a=new int[2];
		int[] g=new int[4];
		int[] Y=new int[3];
		//g(x)的系数
		for (int i = 0; i < 2; i++) {
			a[i]=i+1;
			//产生0-10的两个随机数
			//	 Random r=new Random();
			//	a[i]=r.nextInt(10)+1;
			//	System.out.print("g(x)系数a"+i+"="+" "+a[i]+"   ");
		}
		//System.out.println();
		//g(x)的函数值g0,g1,g2,g3
		for (int i = 0; i <4; i++) {
			g[i]=(a[1]*X[i]*X[i]+a[0]*X[i]+k)%17;
			System.out.print("g"+i+"="+""+g[i]+"   ");
		}
		System.out.println();
//随机输出三个函数值
		int[] reult2 = sj(0,3,3);
		for (int i = 0,j=0; i < reult2.length; i++,j++) {
			int q=reult2[i]+1;
			Y[j]=q*100+g[reult2[i]];
			System.out.print("随机输出三个函数值Y"+reult2[i]+"  "+Y[j]+"  ");
		}
		return(Y);
	}

	public static void main(String[] args) {
		int X[]={1,4,15,1};
		int[] Y=new int[3];
//对X（密钥流）中=相同的数进行
		for (int i = 0; i < 3; i++) {
			for (int j = i+1; j < 4; j++) {
				if(X[i]==X[j]){
					X[j]=X[j]+1;
				}
			}
		}
//产生三个随机函数值 （秘密8）	函数值重复的话就重新计算
		Y= gx( X,8);
		for (int i = 0; i < 2; i++) {
			for (int j = i+1; j < 3; j++) {
				if(X[i]%100==X[j]%100){
					X[j]=X[j]+1;
				}
			}
		}
	}
}
