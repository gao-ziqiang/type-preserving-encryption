package util;

public class ProductKey {
	public static int[] GenerateKeys(int[] LFSR_S,int[] BRC_X,int[] F_R,int KeyLen)
	{
		int[] key=new int[KeyLen];
		//丢弃F的输出w
		{
			Converter.BitReorganization(LFSR_S, BRC_X);
			Converter.F(BRC_X, F_R);
			Converter.LFSRWithWorkMode(LFSR_S);
		}
		for (int i = 0; i < KeyLen; i ++)
		{
			Converter.BitReorganization(LFSR_S, BRC_X);
			key[i]=Converter.F(BRC_X, F_R)^BRC_X[3];
			Converter.LFSRWithWorkMode(LFSR_S);
		}
		return key;
	}
}
