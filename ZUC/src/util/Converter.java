package util;

import converter.MyConverter;

public class Converter {


	/**
	 * 实现对操作数x的循环左移y位功能
	 *  <<  --左操作数按位左移，右操作数指定移动的位数
	 *  >>  --左操作数按位右移，右操作数指定移动的位数
	 *  >>> --左操作数按位右移右操作数指定的位数，左边 高位空出位用0填充
	 * @param x
	 * @param y
	 * @return
	 */
	public static int Rot1(int x,int y)
	{
		return x<<y|x>>>(32-y);
	}


	/**
	 * 线性变换1
	 * @param x
	 * @return
	 */
	public static int L1(int x)
	{
		return (x^Rot1(x, 2)^Rot1(x, 10)^Rot1(x, 18)^Rot1(x, 24));
	}


	/**
	 * 线性变换2
	 * @param x
	 * @return
	 */
	public static int L2(int x) {
		return (x^Rot1(x, 8)^Rot1(x, 14)^Rot1(x, 22)^Rot1(x, 30));
	}

	/**
	 * 将取出的4个8位数值连接在一起
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @return
	 */
	public static int MAKE32(int a, int b, int c, int d) {
		return ((a << 24) | (b << 16)| (c << 8) | (d));
	}

	/**
	 * 非线性变换F
	 * @param BRC_X
	 * @param F_R
	 * @return
	 */
	public static int F(int[] BRC_X,int[] F_R)
	{
		int W, W1, W2, u, v;
		W = (BRC_X[0] ^ F_R[0]) + F_R[1];
		W1 = F_R[0] + BRC_X[1];
		W2 = F_R[1] ^ BRC_X[2];
		u = L1(((W1 << 16)&0xFFFF0000) | ((W2 >> 16)&0xFFFF));//将32位的W1和W2的高、低16位重组
		v = L2(((W2 << 16)&0xFFFF0000) | ((W1 >> 16)&0xFFFF));
		F_R[0] = MAKE32(MyConverter.S0[u >> 24& 0xFF], MyConverter.S1[(u >> 16& 0xFF) ],MyConverter.S0[(u >> 8& 0xFF) ], MyConverter.S1[u & 0xFF]);//作用是分别取出每两位。例：8a4f07bd ，分别取出8a、4f、07、bd
		F_R[1] = MAKE32(MyConverter.S0[v >> 24& 0xFF], MyConverter.S1[(v >> 16& 0xFF) ],MyConverter.S0[(v >> 8& 0xFF) ], MyConverter.S1[v & 0xFF]);
		return W;
	}


	/**
	 * 比特重组
	 * @param LFSR_S
	 * @param BRC_X
	 */
	public static void BitReorganization(int[] LFSR_S,int[] BRC_X)
	{
		BRC_X[0] = ((LFSR_S[15] & 0x7FFF8000) << 1) | (LFSR_S[14] & 0xFFFF);
		BRC_X[1] = ((LFSR_S[11] & 0xFFFF) << 16) | (LFSR_S[9] >> 15);
		BRC_X[2] = ((LFSR_S[7] & 0xFFFF) << 16) | (LFSR_S[5] >> 15);
		BRC_X[3] = ((LFSR_S[2] & 0xFFFF) << 16) | (LFSR_S[0] >> 15);
	}


	/**
	 * 密钥装载
	 * si=ki||di||ivi。位数31=8+15+8
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public static int MAKE31(int a,int b,int c)
	{
		return (a<<23)|(b<<8)|c;
	}


	/**
	 * 初始化阶段
	 * @param k 初始密钥
	 * @param iv 初始向量
	 * @param LFSR_S 16个LFSR寄存器
	 * @param F_R F函数的2个记忆单元
	 * @param BRC_X 比特重组模块
	 */
	public static void Initialization(int[] k, int[] iv,int[] LFSR_S,int[] F_R,int[] BRC_X)
	{
		int w, nCount;
		//密钥装载
		for (int i = 0; i < LFSR_S.length; i++) {
			LFSR_S[i]=MAKE31(k[i], MyConverter.EK_d[i], iv[i]);
		}
		//F中32bit存储单元设为0
		F_R[0]=0;
		F_R[1]=0;
		nCount = 32;
		//进行32次 比特重组 非线性函数F 初始化模式
		while (nCount > 0)
		{
			BitReorganization(LFSR_S,BRC_X);
			w = F(BRC_X,F_R);
			LFSRWithInitialisationMode((w >> 1)&0x7FFFFFFF,LFSR_S);
			nCount --;
		}
	}
	/**
	 * c = a + b mod (2^31 – 1)
	 * @param a
	 * @param b
	 * @return
	 */
	public static int AddM(int a, int b)
	{
		int c = a + b;
		if ((c & 0x80000000)!=0)
		{
			c = (c & 0x7FFFFFFF) + 1;
		}
		return c;
	}

	/**
	 * 有限域2^31-1上的模乘
	 * @param x
	 * @param k
	 * @return
	 */
	public static int MulByPow2(int x, int k)
	{

		return ((((x) << k) | ((x) >> (31 - k))) & 0x7FFFFFFF);
	}

	/**
	 * 初始化模式
	 * @param u
	 */
	public static void LFSRWithInitialisationMode(int u,int[] LFSR_S)
	{
		int f, v;

		f = LFSR_S[0];
		v = MulByPow2(LFSR_S[0], 8);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[4], 20);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[10], 21);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[13], 17);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[15], 15);
		f = AddM(f, v);

		f = AddM(f, u);

		/* update the state 更新寄存器状态 */
		for (int i = 0; i < LFSR_S.length-1; i++) {
			LFSR_S[i] = LFSR_S[i+1];
		}

		LFSR_S[15]=f;
		if (LFSR_S[15] == 0)
		{
			LFSR_S[15] = 0x7FFFFFFF;
		}
	}
	/**
	 * 工作模式
	 */
	public static void LFSRWithWorkMode(int[] LFSR_S)
	{
		int f, v;

		f = LFSR_S[0];
		v = MulByPow2(LFSR_S[0], 8);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[4], 20);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[10], 21);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[13], 17);
		f = AddM(f, v);

		v = MulByPow2(LFSR_S[15], 15);
		f = AddM(f, v);

		//更新寄存器状态
		for (int i = 0; i < LFSR_S.length-1; i++) {
			LFSR_S[i]=LFSR_S[i+1];
		}
		LFSR_S[15] = f;
	}


	public static int[] Yihuo(int[] m,int[] keys)
	{

		long  a[]=new long[keys.length];
		long b[]=new long[keys.length];
		for (int i = 0; i < keys.length; i++) {
			System.out.println("keys是"+Integer.toHexString(keys[i]));

			//	System.out.println("异或结果是"+Integer.toHexString(keys[i]^m[i]));
			//	int m2=Addm(keys[i]^m[i]);

			//	System.out.println("异或结果是"+Integer.toHexString(keys[i]^m[i]));
			int m2=Addm(keys[i]);

			//加密
			a[i]=(m[i]+m2)%10;

			//解密
			//	a[i]=(m[i]-m2)%10;
			if( a[i]<0){
				a[i]=a[i]+10;
			}

			System.out.println("密文"+a[i]);

			System.out.println("  ");
		}
		for (int i = 0; i < a.length; i++) {
			b[i]=a[i];
			b[0]=1;
			System.out.print(b[i]);
		}
		System.out.println();
		return keys;
	}

	//取出一个密钥每个数，转成十六进制后相加
	public static int  Addm(int keys){
		//输出第1位
		int a1=keys >>28;
		a1= a1 & 0x00f;
		System.out.print(a1);
		//输出第2位
		int a2=keys >>24;
		a2= a2 & 0x00f;
		System.out.print("  "+a2);
		//输出第3位
		int a3=keys >>20;
		a3= a3 & 0x00f;
		System.out.print("  "+a3);
		//输出第4位
		int a4=keys >>16;
		a4= a4 & 0x00f;
		System.out.print("  "+a4);
		//输出第5位
		int  a5=keys >>12;
		a5= a5 & 0x00f;
		System.out.print("  "+a5);
		//输出第6位
		int a6=keys >> 8;
		a6= a6 & 0x00f;
		System.out.print("  "+a6);
		//输出第7位
		int a7=keys>>4;
		a7= a7 & 0x00f;
		System.out.print("  "+a7);
		//输出第8位
		int a8=keys & 0x00f;
		System.out.print("  "+a8);
		System.out.println();
		int w=a1+a2+a3+a4+a5+a6+a7+a8;
		// System.out.println(w);
		return(w);
	}

	public static void main(String[] args) {

		//	int[] key={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
		//	int[] iv={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

		//	int[] key={0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
		//	int[] iv={0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
		int[] key={0x3d,0x4c,0x4b,0xe9,0x6a,0x82,0xfd,0xae,0xb5,0x8f,0x64,0x1d,0xb1,0x7b,0x45,0x5b};
		int[] iv={0x84,0x31,0x9a,0xa8,0xde,0x69,0x15,0xca,0x1f,0x6b,0xda,0x6b,0xfb,0xd8,0xc7,0x66};
		int[] LFSR_S=new int[16];
		int[] F_R=new int[2];
		int[] BRC_X=new int[4];
		Initialization(key, iv, LFSR_S, F_R, BRC_X);

		//	int[] m1={1,5,2,1,8,1,9,0,6,9,4};
		int[] m1={1,8,7,2,2,7,9,3,5,4,3};


		int[] keys = ProductKey.GenerateKeys(LFSR_S, BRC_X, F_R,2);

		Yihuo(m1,keys);
		for (int i = 0; i < keys.length; i++) {
			//	Yihuo(mi, keys);
		}
	}
}
