package sm3_256;

import org.bouncycastle.util.encoders.Hex;

public class Credit {
	private String Left,Right;
	private static final String K="28963806";
	private static final int round=6;

	public Credit() {

	}

	public String sm3andKey(String sub)
	{
		int a=0,b=0,m=0;
		int subtext=Integer.parseInt(sub);
		int text[]=new int[sub.length()];
		String key=K.substring(0, sub.length());
		int keytext=Integer.parseInt(key);
		int sk[]=new int[key.length()];
		int middle[]=new int[sub.length()];
		while(keytext!=0)
		{
			sk[a]=keytext%10;
			keytext=keytext/10;
			a++;
		}
		while(subtext!=0)
		{
			text[b]=subtext%10;
			subtext=subtext/10;
			b++;
		}
		for(int i=0;i<sub.length();i++)
		{
			middle[i]=(text[i]+sk[i])%10;
			//System.out.println(middle[j]+"\n");
		}
		for(int j=0;j<sub.length();j++)
		{
			sk[j]=0;
			text[j]=0;
		}
		for(int j=sub.length()-1;j>=0;j--)
		{
			m=(int) (m+middle[j]*(Math.pow(10, j)));
		}
		byte[] md=new byte[32];
		byte[] msg1=Integer.toString(m).getBytes();
		SM3Digest sm3=new SM3Digest();
		sm3.update(msg1, 0, msg1.length);
		sm3.doFinal(md, 0);
		String target=new String(Hex.encode(md));
		target=target.toUpperCase();
		return target;
	}

	public String creditEncry(String s,int flag)
	{
		int odd=0,even=0;
		int checkout;
		String afterencry;
		String range="99999999";
		String sign=s.substring(0, 6);
		String encry=s.substring(6, s.length()-1);
		if(s.length()%2==0)
		{
			Left=encry.substring(0, encry.length()/2);
			Right=encry.substring(encry.length()/2);
		}
		else
		{
			Left=encry.substring(0, encry.length()/2-1);
			Right=encry.substring(encry.length()/2-1);
		}
		//System.out.println(Left+"\n");
		//System.out.println(Right+"\n"); 
		if(flag==0)
		{
			for(int i=1;i<round;i++)//加密
			{
				String middle1=Right;
				String mr=sm3andKey(Right);
				//System.out.println(mr+"\n");
				int right=(Integer.parseInt(Left)+Util.hexToDecimal(mr.substring(0, Left.length())))%(Integer.parseInt(range.substring(0, Left.length()))+1);
				Right=String.format("%0"+Left.length()+"d", right);
				Left=middle1;
				System.out.println(Left+"\n");
				System.out.println(Right+"\n");
			}
		}
		else if(flag==1)
		{
			for(int i=1;i<round;i++)//解密
			{
				String middle1=Right;
				String mr=sm3andKey(Right);
				//System.out.println(mr+"\n");
				int right=(Integer.parseInt(Left)-Util.hexToDecimal(mr.substring(0, Left.length())))%(Integer.parseInt(range.substring(0, Left.length()))+1);
				if(right<0)
				{
					right=right+(Integer.parseInt(range.substring(0, Left.length()))+1);
				}
				Right=String.format("%0"+Left.length()+"d", right);
				Left=middle1;
				//System.out.println(Left+"\n");
				//System.out.println(Right+"\n");
			}
		}
		afterencry=sign+Right+Left;
		if(s.length()%2!=0)
		{
			for(int i=0;i<=afterencry.length()-2;i=i+2)
			{
				odd=odd+Integer.parseInt(afterencry.substring(i, i+1));
			}
			for(int j=1;j<afterencry.length();j=j+2)
			{
				int m=2*Integer.parseInt(afterencry.substring(j, j+1));
				if(m>9)
				{
					m=m-9;
				}
				even=even+m;
			}
		}
		else
		{
			for(int i=0;i<afterencry.length();i=i+2)
			{
				int m=2*Integer.parseInt(afterencry.substring(i, i+1));
				if(m>9)
				{
					m=m-9;
				}
				odd=odd+m;
			}
			for(int j=1;j<=afterencry.length()-2;j=j+2)
			{
				even=even+Integer.parseInt(afterencry.substring(j, j+1));
			}
		}
		if((odd+even)%10==0)
		{
			checkout=0;
		}
		else
		{
			checkout=10-(odd+even)%10;
		}
		return afterencry+Integer.toString(checkout);
	}

}
