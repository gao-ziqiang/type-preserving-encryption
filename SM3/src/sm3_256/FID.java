package sm3_256;

import org.bouncycastle.util.encoders.Hex;

public class FID {
	private static final String K="28963806";
	private static final int round=6;
	private int[] lk=new int[6];
	private int[] rk=new int[7];
	private int[] L=new int[6];
	private int[] R=new int[7];
	private String left1,left2,right1,right2,right3,Left,Right;
	private static final int number[]=new int[] {0,520100,520101,520102,520103,520111,
			520112,520113,520121,520122,520123,520181,520200,520201,520202,520203,520221,
			520300,520301,520302,520321,520322,520323,520324,520325,520326,520327,520328,
			520329,520330,520381,520382,522200,522201,522222,522223,522224,522225,522226,
			522227,522228,522229,522230,522300,522301,522322,522323,522324,522325,522326,
			522327,522328,522400,522401,522422,522423,522424,522425,522426,522427,522428,
			522500,522501,522526,522527,522528,522529,522530,522600,522601,522622,522623,
			522624,522625,522626,522627,522628,522629,522630,522631,522632,522633,522634,
			522635,522636,522700,522701,522702,522722,522723,522725,522726,522727,522728,
			522729,522730,522731,522732

	};

	public FID() {

	}

	public void key() {
		int LK,RK;
		int a=0,b=0;
		LK=Integer.parseInt(K.substring(0, 6));
		RK=Integer.parseInt(K.substring(0, 7));
		while(LK!=0)
		{
			lk[a]=LK%10;
			LK=LK/10;
			a++;
		}
		while(RK!=0)
		{
			rk[b]=RK%10;
			RK=RK/10;
			b++;
		}
	}

	public String idKey(String cleartext)
	{
		int i=0,m=0;
		String result;
		int text=Integer.parseInt(cleartext);
		//System.out.println(text+"\n");
		//System.out.println(cleartext.length()+"\n");
		int middle[]=new int[cleartext.length()];
		if(cleartext.length()==7)
		{
			while(text!=0) {
				R[i]=text%10;
				//System.out.println(L[i]+"\n");
				text=text/10;
				i++;
				//System.out.println(cleartext+"\n");
			}
			for(int j=0;j<cleartext.length();j++)
			{
				middle[j]=(R[j]+rk[j])%10;
				//System.out.println(middle[j]+"\n");
			}
			for(int n=0;n<cleartext.length();n++)
			{
				R[n]=0;
			}
		}
		else
		{
			while(text!=0) {
				L[i]=text%10;
				//System.out.println(R[i]+"\n");
				text=text/10;
				i++;
			}
			//System.out.print("\n");
			for(int j=0;j<cleartext.length();j++)
			{
				middle[j]=(L[j]+lk[j])%10;
				//System.out.print(middle[j]);
			}
			for(int n=0;n<cleartext.length();n++)
			{
				L[n]=0;
			}

		}

		for(int j=cleartext.length()-1;j>=0;j--)
		{
			m=(int) (m+middle[j]*(Math.pow(10, j)));
		}
		if(cleartext.length()==7)
		{
			result=String.format("%07d", m);
		}
		else
		{
			result=String.format("%06d", m);
		}
		//System.out.print(result);
		//System.out.print("\n");
		return result;
	}

	public String idencrySM3(String m)
	{
		//String source=Integer.toString(m);
		byte[] md=new byte[32];
		byte[] msg1=m.getBytes();
		SM3Digest sm3=new SM3Digest();
		sm3.update(msg1, 0, msg1.length);
		sm3.doFinal(md, 0);
		String target=new String(Hex.encode(md));
		target=target.toUpperCase();
		//System.out.print(target+"\n");
		if(m.length()==7)
		{
			String part=target.substring(0, 6);
			left1=Left.substring(0, 2);
			left2=Left.substring(2, 6);
			//System.out.print(part+"\n");
			//System.out.print(left1+"\n");
			//System.out.print(left2+"\n");
			left1=String.format("%02d", 1+(Integer.parseInt(left1)-1+Util.hexToDecimal(part.substring(0, 2)))%97);
			left2=String.format("%04d", 1984+(Integer.parseInt(left2)-1984+Util.hexToDecimal(part.substring(2, 6)))%35);
			//System.out.print(left1+"\n");
			//System.out.print(left2+"\n");
			Left=left1+left2;
			return Left;
		}
		else
		{
			String part=target.substring(0, 7);
			right1=Left.substring(0, 2);
			right2=Left.substring(2, 4);
			right3=Left.substring(4, 7);
			//System.out.print(part1);
			//System.out.print("\n");
			//System.out.print(part2);
			//System.out.print("\n");
			right1=String.format("%02d", 1+(Integer.parseInt(right1)-1+Util.hexToDecimal(part.substring(0, 2)))%12);
			right2=String.format("%02d", 1+(Integer.parseInt(right2)-1+Util.hexToDecimal(part.substring(2, 4)))%28);
			right3=String.format("%03d", (Integer.parseInt(right3)+Util.hexToDecimal(part.substring(4, 7)))%1000);
			Right=right1+right2+right3;
			return Right;
		}
	}

	public String iddecrySM3(String m)
	{
		//String source=Integer.toString(m);
		byte[] md=new byte[32];
		byte[] msg1=m.getBytes();
		SM3Digest sm3=new SM3Digest();
		sm3.update(msg1, 0, msg1.length);
		sm3.doFinal(md, 0);
		String target=new String(Hex.encode(md));
		target=target.toUpperCase();
		//System.out.print(target+"\n");
		if(m.length()==7)
		{
			String part=target.substring(0, 6);
			left1=Left.substring(0, 2);
			left2=Left.substring(2, 6);
			//System.out.print(part+"\n");
			//System.out.print(left1+"\n");
			//System.out.print(left2+"\n");
			int m1=(Integer.parseInt(left1)-1-Util.hexToDecimal(part.substring(0, 2)))%97;
			int m2=(Integer.parseInt(left2)-1984-Util.hexToDecimal(part.substring(2, 6)))%35;
			if(m1<0)
			{
				left1=String.format("%02d", 1+m1+97);
			}
			else
			{
				left1=String.format("%02d", 1+m1);
			}
			if(m2<0)
			{
				left2=String.format("%04d", 1984+m2+35);
			}
			else
			{
				left2=String.format("%04d", 1984+m2);
			}
			//System.out.print(left1+"\n");
			//System.out.print(left2+"\n");
			Left=left1+left2;
			return Left;
		}
		else
		{
			String part=target.substring(0, 7);
			right1=Left.substring(0, 2);
			right2=Left.substring(2, 4);
			right3=Left.substring(4, 7);
			//System.out.print(part1);
			//System.out.print("\n");
			//System.out.print(part2);
			//System.out.print("\n");
			//String left=String.format("%08d", Left);
			int m1=(Integer.parseInt(right1)-1-Util.hexToDecimal(part.substring(0, 2)))%12;
			int m2=(Integer.parseInt(right2)-1-Util.hexToDecimal(part.substring(2, 4)))%28;
			int m3=(Integer.parseInt(right3)-Util.hexToDecimal(part.substring(4, 7)))%1000;
			if(m1<0)
			{
				right1=String.format("%02d", 1+m1+12);
			}
			else
			{
				right1=String.format("%02d", 1+m1);
			}
			if(m2<0)
			{
				right2=String.format("%02d", 1+m2+28);
			}
			else
			{
				right2=String.format("%02d", 1+m2);
			}
			if(m3<0)
			{
				right3=String.format("%03d", m3+1000);
			}
			else
			{
				right3=String.format("%03d", m3);
			}
			Right=right1+right2+right3;
			return Right;
		}
	}

	public String Idfeistel(String s,int flag)
	{
		int checkout=0;
		int a[]=new int[17];
		int w[]=new int[] {7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2};
		String result,endresult;
		String address="0",year;
		Left=s.substring(0, 6);
		//System.out.println(Left+"\n");
		for(int i=0;i<98;i++)
		{
			if(Integer.parseInt(Left)==number[i])
			{
				Left=String.format("%02d", i);
			}
		}
		Left=Left+s.substring(6, 10);
		//System.out.println(Left+"\n");
		Right=s.substring(10, 17);
		//System.out.println(Right+"\n");
		if(flag==0)
		{
			for(int i=1;i<round;i++)
			{
				if(i%2!=0)
				{
					String middle1=Right;
					String mr=idKey(Right);
					//System.out.println(mr+"\n");
					Right=idencrySM3(mr);
					Left=middle1;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}
				else
				{
					String middle2=Right;
					String ml=idKey(Right);
					//System.out.println(ml+"\n");
					Right=idencrySM3(ml);
					Left=middle2;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}
			}
		}
		else if(flag==1)
		{
			for(int i=1;i<round;i++)
			{
				if(i%2!=0)
				{
					String middle1=Right;
					String mr=idKey(Right);
					//System.out.println(mr+"\n");
					Right=iddecrySM3(mr);
					Left=middle1;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}
				else
				{
					String middle2=Right;
					String ml=idKey(Right);
					//System.out.println(ml+"\n");
					Right=iddecrySM3(ml);
					Left=middle2;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}

			}
		}
		year=Right.substring(2);
		//System.out.println(year+"\n");
		//System.out.println(Right.substring(0, 2));
		for(int j=1;j<98;j++)
		{
			if(Integer.parseInt(Right.substring(0, 2))==j)
			{
				address=Integer.toString(number[j]);
				//System.out.println(address+"\n");
			}
			//System.out.println(address+"\n");
		}
		result=address+year+Left;
		//System.out.println(result+"\n");
		for(int i=0;i<17;i++)
		{
			a[i]=Integer.parseInt(result.substring(i, i+1));
		}
		for(int j=0;j<17;j++)
		{
			checkout=checkout+w[j]*a[j];
		}
		//System.out.println(checkout+"\n");
		checkout=(12-(checkout%11))%11;
		if(checkout==10)
		{
			//System.out.println(result+"X"+"\n");
			endresult=result+"X";
		}
		else
		{
			//System.out.println(result+Integer.toString(checkout)+"\n");
			endresult=result+Integer.toString(checkout);
		}
		return endresult;
	}
}
