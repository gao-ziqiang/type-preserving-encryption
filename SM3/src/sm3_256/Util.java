package sm3_256;

import java.math.BigInteger;

public class Util {
	//整形转换成网络传输的字节流(字节数组)型数据 @param num 一个整形数据 @return 4个字节的字节数组
	public static byte[] intToBytes(int num)
	{
		byte[] bytes=new byte[4];
		bytes[0]=(byte)(0xff&(num>>0));
		bytes[1]=(byte)(0xff&(num>>8));
		bytes[2]=(byte)(0xff&(num>>16));
		bytes[3]=(byte)(0xff&(num>>24));
		return bytes;
	}

	//四个字节的字节数据转换成一个整形数据 @param bytes 4个字节的字节数组 @return 一个整形数据
	public static int byteToInt(byte[] bytes)
	{
		int num=0;
		int temp;
		temp=(0x000000ff&(bytes[0]))<<0;
		num=num|temp;
		temp=(0x000000ff&(bytes[1]))<<8;
		num=num|temp;
		temp=(0x000000ff&(bytes[2]))<<16;
		num=num|temp;
		temp=(0x000000ff&(bytes[3]))<<24;
		num=num|temp;
		return num;
	}

	//长整形转换成网络传输的字节流(字节数组)型数据 @param num一个长整形数据 @return 4个字节的字节数组
	public static byte[] longToBytes(long num)
	{
		byte[] bytes=new byte[8];
		for(int i=0;i<8;i++)
		{
			bytes[i]=(byte)(0xff&(num>>(i*8)));
		}
		return bytes;
	}

	//十六进制转换为十进制
	public static int hexToDecimal(String hex)
	{
		int decimal=(int) Long.parseLong(hex, 16);
		return decimal;
	}



}
