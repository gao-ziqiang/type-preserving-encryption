package sm3_256;

import org.bouncycastle.util.encoders.Hex;

public class FF {
	private int LK,RK;
	private String Left,Right;
	private int round=6;
	private int lk[]=new int[2];
	private int rk[]=new int[8];
	private int L[]=new int[2];
	private int R[]=new int[8];
	private static final String K="28963806";
	private static final int number[]=new int[] {0,133,149,153,173,177,180,181,189,199,130,
			131,132,145,155,156,171,175,176,185,
			186,166,134,135,136,137,138,139,147,150,
			151,152,157,158,159,172,178,182,183,184,
			187,188,198};

	public FF() {

	}

	public void key()
	{
		int i=0,j=0;
		RK=Integer.parseInt(K);
		//System.out.println(RK+"\n");
		LK=Integer.parseInt(K.substring(0, 2));
		//System.out.println(LK+"\n");
		//int j=K.length()-1;
		//int i=K.substring(0, 3).length()-1;
		while(LK!=0)
		{
			lk[i]=LK%10;
			LK=LK/10;
			i++;
		}
		while(RK!=0)
		{
			rk[j]=RK%10;
			RK=RK/10;
			j++;
		}
	}

	public String secretKey(String cleartext)
	{
		int i=0,m=0;
		String result;
		int text=Integer.parseInt(cleartext);
		//System.out.println(text+"\n");
		//System.out.println(cleartext.length()+"\n");
		int middle[]=new int[cleartext.length()];
		if(cleartext.length()!=8)
		{
			while(text!=0) {
				L[i]=text%10;
				//System.out.println(L[i]+"\n");
				text=text/10;
				i++;
				//System.out.println(cleartext+"\n");
			}
			for(int j=0;j<cleartext.length();j++)
			{
				middle[j]=(L[j]+lk[j])%10;
				//System.out.println(middle[j]+"\n");
			}
			for(int n=0;n<cleartext.length();n++)
			{
				L[n]=0;
			}
		}
		else
		{
			while(text!=0) {
				R[i]=text%10;
				//System.out.println(R[i]+"\n");
				text=text/10;
				i++;
			}
			//System.out.print("\n");
			for(int j=0;j<cleartext.length();j++)
			{
				middle[j]=(R[j]+rk[j])%10;
				//System.out.print(middle[j]);
			}
			for(int n=0;n<cleartext.length();n++)
			{
				R[n]=0;
			}
		}

		for(int j=cleartext.length()-1;j>=0;j--)
		{
			m=(int) (m+middle[j]*(Math.pow(10, j)));
		}
		if(cleartext.length()!=8)
		{
			result=String.format("%02d", m);
		}
		else
		{
			result=String.format("%08d", m);
		}
		//System.out.print(result);
		//System.out.print("\n");
		return result;
	}

	public String applySM3(String m)
	{
		//String source=Integer.toString(m);
		byte[] md=new byte[32];
		byte[] msg1=m.getBytes();
		SM3Digest sm3=new SM3Digest();
		sm3.update(msg1, 0, msg1.length);
		sm3.doFinal(md, 0);
		String target=new String(Hex.encode(md));
		target=target.toUpperCase();
		//System.out.print(target);
		//System.out.print("\n");
		if(m.length()==8)
		{
			String part=target.substring(0, 2);
			//System.out.print(part);
			//System.out.print("\n");
			//System.out.print(Left/10);
			//System.out.print("\n");
			Left=String.format("%02d",1+(Integer.parseInt(Left)-1+Util.hexToDecimal(part))%42);
			return Left;
		}
		else
		{
			String part1=target.substring(0, 4);
			String part2=target.substring(4, 8);
			//System.out.print(part1);
			//System.out.print("\n");
			//System.out.print(part2);
			//System.out.print("\n");
			//String left=String.format("%08d", Left);
			int left1=Integer.parseInt(Left.substring(0, 4));
			int left2=Integer.parseInt(Left.substring(4));
			left1=(left1+Util.hexToDecimal(part1))%10000;
			left2=(left2+Util.hexToDecimal(part2))%10000;
			String resultleft1=String.format("%04d", left1);
			String resultleft2=String.format("%04d", left2);
			Right=resultleft1+resultleft2;
			//Right=Integer.toString(left1*10000+left2);
			return Right;
		}
	}

	public String decrySM3(String m)
	{
		//String source=Integer.toString(m);
		byte[] md=new byte[32];
		byte[] msg1=m.getBytes();
		SM3Digest sm3=new SM3Digest();
		sm3.update(msg1, 0, msg1.length);
		sm3.doFinal(md, 0);
		String target=new String(Hex.encode(md));
		target=target.toUpperCase();
		//System.out.print(target);
		//System.out.print("\n");
		if(m.length()==8)
		{
			String part=target.substring(0, 2);
			//System.out.print(part);
			//System.out.print("\n");
			//System.out.print(Left/10);
			//System.out.print("\n");
			int s=(Integer.parseInt(Left)-1-Util.hexToDecimal(part))%42;
			if(s<0)
			{
				Left=String.format("%02d", 1+s+42);
			}
			else
			{
				Left=String.format("%02d", 1+s);
			}
			return Left;
		}
		else
		{
			String part1=target.substring(0, 4);
			String part2=target.substring(4, 8);
			//System.out.print(part1);
			//System.out.print("\n");
			//System.out.print(part2);
			//System.out.print("\n");
			int left1=Integer.parseInt(Left.substring(0, 4));
			int left2=Integer.parseInt(Left.substring(4));
			int l1=(left1-Util.hexToDecimal(part1))%10000;
			int l2=(left2-Util.hexToDecimal(part2))%10000;
			//System.out.print(l1);
			//System.out.print("\n");
			//System.out.print(l2);
			//System.out.print("\n");
			if(l1<0)
			{
				left1=l1+10000;
			}
			else
			{
				left1=l1;
			}
			if(l2<0)
			{
				left2=l2+10000;
			}
			else
			{
				left2=l2;
			}
			//System.out.print(left1);
			//System.out.print("\n");
			//System.out.print(left2);
			//System.out.print("\n");
			String resultleft1=String.format("%04d", left1);
			String resultleft2=String.format("%04d", left2);
			Right=resultleft1+resultleft2;
			//Right=Integer.toString(left1*10000+left2);
			return Right;
		}
	}

	public String Ffeistel(String s,int flag)
	{
		if (s == null){
			throw new RuntimeException("s不能为空");
		}
		Left=s.substring(0, 3);
		//System.out.println(Left+"\n");
		for(int i=0;i<43;i++)
		{
			if(Integer.parseInt(Left)==number[i])
			{
				Left=String.format("%02d", i);
			}
		}
		//System.out.println(Left+"\n");
		Right=s.substring(3);
		//System.out.println(Right+"\n");
		if(flag==0)//加密
		{
			for(int i=1;i<round;i++)
			{
				if(i%2!=0)
				{
					String middle1=Right;
					String mr=secretKey(Right);
					//System.out.println(mr+"\n");
					Right=applySM3(mr);
					Left=middle1;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}
				else
				{
					String middle2=Right;
					String ml=secretKey(Right);
					//System.out.println(ml+"\n");
					Right=applySM3(ml);
					Left=middle2;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}

			}
		}
		else if(flag==1)//解密
		{
			for(int i=1;i<round;i++)
			{
				if(i%2!=0)
				{
					String middle1=Right;
					String mr=secretKey(Right);
					//System.out.println(mr+"\n");
					Right=decrySM3(mr);
					Left=middle1;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}
				else
				{
					String middle2=Right;
					String ml=secretKey(Right);
					//System.out.println(ml+"\n");
					Right=decrySM3(ml);
					Left=middle2;
					//System.out.println(Left+"\n");
					//System.out.println(Right+"\n");
				}
			}
		}
		for(int j=0;j<43;j++)
		{
			if(Integer.parseInt(Right)==j)
			{
				Right=Integer.toString(number[j]);
			}
		}
		return Right+Left;
	}
}
