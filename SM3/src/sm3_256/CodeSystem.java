package sm3_256;

import java.awt.BorderLayout;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sun.media.jfxmedia.logging.Logger;

import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import jxl.*;
import java.io.*;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

public class CodeSystem extends JFrame {

	//窗体元素
	private JPanel contentPane;
	private JComboBox<String> databaseCombo;
	private String selectdatabase;
	private JLabel lblNewLabel_1;
	private JComboBox<String> tableCombo;
	private String selecttable;
	private JLabel lblNewLabel_2;
	private JComboBox<String> natureCombo;
	private String selectnature;

    //判断有没有被加密
	private int flag;

	//文件相关
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private HSSFRow row1;
	private static String phonefileDir="D:\\test\\phone.xls";
	private static String idfileDir="D:\\test\\idcard.xls";
	private static String cardfileDir="D:\\test\\bankcard.xls";
	private File phonefile=new File(phonefileDir);
	private File idfile=new File(idfileDir);
	private File cardfile=new File(cardfileDir);
	//从文件中读取出来的数据
	private String enphone[]=new String[40];
	private String enid[]=new String[40];
	private String encard[]=new String[40];

	/**
	 * Launch the application.
	 * 程序入口
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CodeSystem frame = new CodeSystem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//创建文件
	public HSSFSheet createSheet(String sheetName,String choosenature)
	{
		String titleRow[]=new String[3];
		workbook=new HSSFWorkbook();
		sheet=workbook.createSheet(sheetName);
		if(choosenature.equals("phone_num"))
		{
			titleRow=new String[] {"phone_num","加密后","解密后"};
		}
		else if(choosenature.equals("id_card"))
		{
			titleRow=new String[] {"id_card","加密后","解密后"};
		}
		else if(choosenature.equals("bank_card"))
		{
			titleRow=new String[] {"bank_card","加密后","解密后"};
		}
		try
		{
			HSSFRow row=workbook.getSheet(sheetName).createRow(0);
			for(short i=0;i<titleRow.length;i++)
			{
				HSSFCell cell=row.createCell(i);
				cell.setCellValue(titleRow[i]);
			}
		}
		catch(Exception e)
		{
		}
		return sheet;
	}

	/**
	 * Create the frame.创建框架。
	 */
	public CodeSystem() {
		if(phonefile.exists())
		{
			phonefile.delete();
		}
		if(idfile.exists())
		{
			idfile.delete();
		}
		if(cardfile.exists())
		{
			cardfile.delete();
		}
		setTitle("\u52A0\u5BC6\u89E3\u5BC6\u7CFB\u7EDF");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("\u9009\u62E9\u6570\u636E\u5E93:");
		lblNewLabel.setBounds(10, 10, 70, 25);
		contentPane.add(lblNewLabel);

		databaseCombo= new JComboBox<String>();
		databaseCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				databaseCombo.removeAllItems();
				String url="jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
				String user="root";
				String password="root";
				Connection con=null;
				Statement stmt=null;
				String databasequery="show databases";
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					con=DriverManager.getConnection(url, user, password);
					stmt=con.createStatement();
					ResultSet rs=stmt.executeQuery(databasequery);
					//System.out.println("查询的信息如下:");
					while(rs.next()) {
						databaseCombo.addItem(rs.getString(1));
						//System.out.println(name+"\n");
					}
				}
				catch(ClassNotFoundException e1) {
					System.err.print("类没有找到异常");
					System.err.println(e1.getMessage());
				}
				catch(SQLException e2) {
					System.err.println("SQL异常:"+e2.getMessage());
				}
				finally {
					if(stmt!=null) {
						try {
							stmt.close();
						}
						catch(SQLException e3) {
						}
						stmt=null;
					}
					if(con!=null) {
						try {
							con.close();
						}
						catch(SQLException e4) {
						}
						con=null;
					}
				}
			}
		});
		databaseCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent databaseevt) {
				if(databaseevt.getStateChange()==ItemEvent.SELECTED)
				{
					selectdatabase=databaseCombo.getSelectedItem().toString();
					//System.out.println(selectdatabase+"\n");

				}
			}
		});
		databaseCombo.setBounds(90, 12, 144, 21);
		contentPane.add(databaseCombo);

		lblNewLabel_1 = new JLabel("\u9009\u62E9\u8868\uFF1A");
		lblNewLabel_1.setBounds(241, 15, 54, 15);
		contentPane.add(lblNewLabel_1);

		tableCombo = new JComboBox<String>();
		tableCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				tableCombo.removeAllItems();
				String url="jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
				String user="root";
				String password="root";
				Connection con=null;
				Statement stmt=null;
				String tablequery="select table_name from information_schema.tables where table_schema='"+selectdatabase+"'";
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					con=DriverManager.getConnection(url, user, password);
					stmt=con.createStatement();
					ResultSet rs=stmt.executeQuery(tablequery);
					//System.out.println("查询的信息如下:");
					while(rs.next()) {
						tableCombo.addItem(rs.getString(1));
						//System.out.println(rs.getString(1)+"\n");
					}
				}
				catch(ClassNotFoundException e1) {
					System.err.print("类没有找到异常");
					System.err.println(e1.getMessage());
				}
				catch(SQLException e2) {
					System.err.println("SQL异常:"+e2.getMessage());
				}
				finally {
					if(stmt!=null) {
						try {
							stmt.close();
						}
						catch(SQLException e3) {
						}
						stmt=null;
					}
					if(con!=null) {
						try {
							con.close();
						}
						catch(SQLException e4) {
						}
						con=null;
					}
				}
			}
		});
		tableCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent tableevt) {
				if(tableevt.getStateChange()==ItemEvent.SELECTED)
				{
					selecttable=tableCombo.getSelectedItem().toString();
					//System.out.println(selecttable+"\n");
				}
			}
		});
		tableCombo.setBounds(289, 12, 119, 21);
		contentPane.add(tableCombo);

		lblNewLabel_2 = new JLabel("\u9009\u62E9\u5C5E\u6027\uFF1A");
		lblNewLabel_2.setBounds(10, 85, 70, 15);
		contentPane.add(lblNewLabel_2);

		natureCombo = new JComboBox<String>();
		natureCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				natureCombo.removeAllItems();
				String url="jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
				String user="root";
				String password="root";
				Connection con=null;
				Statement stmt=null;
				//String databasequery="show databases";
				//String tablequery="select table_name from information_schema.tables where table_schema=selectdatabase";
				String naturequery="select COLUMN_NAME from information_schema.COLUMNS where table_name='"+selecttable+"' and table_schema='"+selectdatabase+"'";
				//String valuequery="select selectnature form selecttable";
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					con=DriverManager.getConnection(url, user, password);
					stmt=con.createStatement();
					ResultSet rs=stmt.executeQuery(naturequery);
					//System.out.println("查询的信息如下:");
					while(rs.next()) {
						natureCombo.addItem(rs.getString(1));
						//System.out.println(name+"\n");
					}
				}
				catch(ClassNotFoundException e1) {
					System.err.print("类没有找到异常");
					System.err.println(e1.getMessage());
				}
				catch(SQLException e2) {
					System.err.println("SQL异常:"+e2.getMessage());
				}
				finally {
					if(stmt!=null) {
						try {
							stmt.close();
						}
						catch(SQLException e3) {
						}
						stmt=null;
					}
					if(con!=null) {
						try {
							con.close();
						}
						catch(SQLException e4) {
						}
						con=null;
					}
				}
			}
		});
		natureCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent natureevt) {
				if(natureevt.getStateChange()==ItemEvent.SELECTED)
				{
					selectnature=natureCombo.getSelectedItem().toString();
				}
				//System.out.println(selectnature);
			}
		});
		natureCombo.setBounds(90, 82, 144, 21);
		contentPane.add(natureCombo);

		JLabel resultlbl = new JLabel("");
		resultlbl.setBounds(150, 193, 180, 35);
		contentPane.add(resultlbl);

		JButton enbtn = new JButton("\u52A0\u5BC6");
		enbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				flag=0;
				//boolean checkflag=false;
				String url="jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
				String user="root";
				String password="root";
				Connection con=null;
				Statement stmt=null;
				String valuequery = null;
				try
				{
					Class.forName("com.mysql.cj.jdbc.Driver");
					con=DriverManager.getConnection(url, user, password);
					stmt=con.createStatement();
					if(selecttable.equals("bank"))
					{
						if(selectnature.equals("phone_num"))
						{
							int i=0;
							FF ff=new FF();
							ff.key();
							String result;
							String sheetName="sheet1";
							valuequery="select phone_num from bank";//查询语句
							ResultSet rs=stmt.executeQuery(valuequery);//执行查询操作
							sheet=createSheet(sheetName,selectnature);
							sheet.setDefaultColumnWidth((short) 15);
							while(rs.next())
							{
								row1=sheet.createRow(i+1);
								row1.createCell(0).setCellValue(rs.getString(1));
								result=ff.Ffeistel(rs.getString(1), flag);
								row1.createCell(1).setCellValue(result);
								enphone[i]=result;
								i++;
								//System.out.println(result+"\n");
							}
							try
							{
								//phonefile=new File(phonefileDir);
								OutputStream fos=new FileOutputStream(phonefile);
								workbook.write(fos);
								System.out.println("写入成功");
								fos.close();
							}catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							resultlbl.setText("加密完成!");
						}
						else if(selectnature.equals("id_card"))
						{
							int i=0;
							FID fid=new FID();
							fid.key();
							String result;
							String sheetName="sheet1";
							valuequery="select id_card from bank";
							ResultSet rs=stmt.executeQuery(valuequery);
							sheet=createSheet(sheetName,selectnature);
							sheet.setDefaultColumnWidth((short) 20);
							while(rs.next())
							{
								row1=sheet.createRow(i+1);
								row1.createCell(0).setCellValue(rs.getString(1));
								//System.out.println(rs.getString(1)+"\n");
								result=fid.Idfeistel(rs.getString(1),flag);
								row1.createCell(1).setCellValue(result);
								enid[i]=result;
								i++;
								//System.out.println(i+"\n");
							}
							try
							{
								//idfile=new File(idfileDir);
								OutputStream fos=new FileOutputStream(idfile);
								workbook.write(fos);
								System.out.println("写入成功");
								fos.close();
							}catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							resultlbl.setText("加密完成!");
						}
						else if(selectnature.equals("bank_card"))
						{
							int i=0;
							Credit credit=new Credit();
							String result;
							String sheetName="sheet1";
							valuequery="select bank_card from bank";
							ResultSet rs=stmt.executeQuery(valuequery);
							sheet=createSheet(sheetName,selectnature);
							sheet.setDefaultColumnWidth((short) 23);
							while(rs.next())
							{
								row1=sheet.createRow(i+1);
								row1.createCell(0).setCellValue(rs.getString(1));
								result=credit.creditEncry(rs.getString(1), flag);
								row1.createCell(1).setCellValue(result);
								encard[i]=result;
								i++;
								//System.out.println(result+"\n");
							}
							try
							{
								//cardfile=new File(cardfileDir);
								OutputStream fos=new FileOutputStream(cardfile);
								workbook.write(fos);
								System.out.println("写入成功");
								fos.close();
							}catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							resultlbl.setText("加密完成!");
						}
					}
					else
					{

						resultlbl.setText("该属性没有对应的加密模型!");
					}
				}
				catch(ClassNotFoundException e1) {
					System.err.print("类没有找到异常");
					System.err.println(e1.getMessage());
				}
				catch(SQLException e2) {
					System.err.println("SQL异常:"+e2.getMessage());
				}
				finally {
					if(stmt!=null) {
						try {
							stmt.close();
						}
						catch(SQLException e3) {
						}
						stmt=null;
					}
					if(con!=null) {
						try {
							con.close();
						}
						catch(SQLException e4) {
						}
						con=null;
					}
				}
			}
		});
		enbtn.setBounds(90, 141, 93, 23);
		contentPane.add(enbtn);

		JButton debtn = new JButton("\u89E3\u5BC6");
		debtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				flag=1;
				String result;
				if(selecttable.equals("bank"))
				{
					if(selectnature.equals("phone_num"))
					{
						if(phonefile.exists())
						{
							FF ff=new FF();
							ff.key();
							for(int i=0;i<enphone.length;i++)
							{
								//HSSFRow row1=sheet.createRow(i+1);
								if (enphone[i] == null){
									break;
								}
//								System.out.println(enphone[i]);
								row1=sheet.getRow(i+1);
								result=ff.Ffeistel(enphone[i], flag);
								row1.createCell(2).setCellValue(result);
							}
							try
							{
								//idfile=new File(idfileDir);
								OutputStream fos=new FileOutputStream(phonefile);
								workbook.write(fos);
								//System.out.println("写入成功");
								fos.close();
							}catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							resultlbl.setText("解密完成!");
						}
						else
						{
							resultlbl.setText("请先将此项加密!");
						}
					}
					else if(selectnature.equals("id_card"))
					{
						if(idfile.exists())
						{
							FID fid=new FID();
							fid.key();
							for(int i=0;i<enid.length;i++)
							{
								//HSSFRow row1=sheet.createRow(i+1);
								if (enid[i] == null){
									break;
								}
								row1=sheet.getRow(i+1);
								result=fid.Idfeistel(enid[i], flag);
								row1.createCell(2).setCellValue(result);
							}
							try
							{
								//idfile=new File(idfileDir);
								OutputStream fos=new FileOutputStream(idfile);
								workbook.write(fos);
								//System.out.println("写入成功");
								fos.close();
							}catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							resultlbl.setText("解密完成!");
						}
						else
						{
							resultlbl.setText("请先将此项加密!");
						}
					}
					else if(selectnature.equals("bank_card"))
					{
						if(cardfile.exists())
						{
							Credit credit=new Credit();
							for(int i=0;i<encard.length;i++)
							{
								//HSSFRow row1=sheet.createRow(i+1);
								if (encard[i] == null){
									break;
								}
								row1=sheet.getRow(i+1);
								result=credit.creditEncry(encard[i], flag);
								row1.createCell(2).setCellValue(result);
							}
							try
							{
								//idfile=new File(idfileDir);
								OutputStream fos=new FileOutputStream(cardfile);
								workbook.write(fos);
								//System.out.println("写入成功");
								fos.close();
							}catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							resultlbl.setText("解密完成!");
						}
						else
						{
							resultlbl.setText("请先将此项加密!");
						}
					}
				}
				else
				{
					resultlbl.setText("请选择结构型数据进行解密!");
				}
			}
		});
		debtn.setBounds(265, 141, 93, 23);
		contentPane.add(debtn);
	}
}
